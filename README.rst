rbac_playbook for Ansible playbooks
===================================

rbac is a script which can control the ability to execute individual playbooks
in a set of many playbooks using POSIX groups.

To decrease the potential bug/attack surface only certain ``ansible-playbook``
arguments are allowed and those are all re-interpreted using argparse.


The command can be used through::

  rbac_playbook.py <playbook name> -C -P 222 

Allowed Arguments
-----------------

  * ``playbook name`` name of the playbook file, relative to the base ansible directory

  * ``-P <port number>`` use an alternate port for communicating with hosts

  * ``-C`` use ``--check`` on the ``ansible-playbook`` call

  * ``-l <inventory>`` use ``--limit`` on ``ansible-playbook`` to limit the hosts used for the operation

  * ``-t <tags>`` use ``--tags`` to limit the execution to tasks/roles/etc/ tagged with ``<tags>``

  * ``-u <user>`` use ``<user>`` on the remote machine through ``ansible-playbook``

  * ``--start-at-task <task>`` start playbook execution at ``<task>``

Configurable Values
-------------------

The possible configuration values are listed in the ``config`` section of the 
``conf/conf.yaml.example`` file. For the configuration file to be picked up,
it must be in ``/etc/rbac`` and named ``conf.yaml``.

Running the Unit Tests
----------------------

There are several tests written for rbac_playbook. To execute these tests, make
sure that you have py.test installed and run::

  py.test test_rbac.py
