import copy

from dingus import Dingus

import rbac_playbook as rbac


class TestCli(object):
    def setup_method(self, method):
        self.ref_settings = {'yamlfile': 'unicorn.yaml',
                             'host': 'pink.unicorn.com',
                             }
    def test_simple_playbook(self):
        ref_command = 'unicorn.yml'
        test_parser = rbac.get_argparser()
        test_args = vars(test_parser.parse_args(ref_command.split()))

        assert test_args['playbook'] == 'unicorn.yml'

    def test_limit_host(self):
        ref_command = "unicorn.yml -l pink.unicorn.com"
        test_parser = rbac.get_argparser()
        test_args = vars(test_parser.parse_args(ref_command.split()))

        assert test_args['playbook'] == 'unicorn.yml'
        assert test_args['limit'] == 'pink.unicorn.com'


    def test_limit_host_multiple(self):
        ref_command = "unicorn.yml -l pink.unicorn.com:white.unicorn.com;blue.unicorn.com,purple.unicorn.com"
        test_parser = rbac.get_argparser()
        test_args = vars(test_parser.parse_args(ref_command.split()))

        assert test_args['playbook'] == 'unicorn.yml'
        assert test_args['limit'] == 'pink.unicorn.com:white.unicorn.com;blue.unicorn.com,purple.unicorn.com'

    def test_args_full_house(self):
        ref_command = "unicorn.yml -l pink.unicorn.com:white.unicorn.com;blue.unicorn.com,purple.unicorn.com -C -u rootie -t tag,tagg;tog:togg --start-at-task \'sometask\'"
        test_parser = rbac.get_argparser()
        test_args = vars(test_parser.parse_args(ref_command.split()))

        assert test_args['playbook'] == 'unicorn.yml'
        assert test_args['limit'] == 'pink.unicorn.com:white.unicorn.com;blue.unicorn.com,purple.unicorn.com'
        assert test_args['check']
        assert test_args['user'] == 'rootie'
        assert test_args['tags'] == 'tag,tagg;tog:togg'
        assert test_args['start_at_task'] == "'sometask'"


class TestProcessArgs(object):

    def test_standard(self):
        ref_command = {"limit": "pink.unicorn.com:white.unicorn.com;blue.unicorn.com,purple.unicorn.com"}

        test_args = rbac.process_args(ref_command)
        assert test_args['limit'] == ['pink.unicorn.com', 'white.unicorn.com', 'blue.unicorn.com', 'purple.unicorn.com']

    def test_tags(self):
        ref_command = {"tags": "tag,tagg;tog:togg"}

        test_args = rbac.process_args(ref_command)
        assert test_args['tags'] == ['tag', 'tagg', 'tog', 'togg']

    def test_tags_none(self):
        ref_command = {"tags": None}

        test_args = rbac.process_args(ref_command)
        assert test_args['tags'] == None

    def test_process_boolean(self):
        ref_command = {"check": True}

        test_args = rbac.process_args(ref_command)
        assert test_args['check'] == True

    def test_string_notlist(self):
        ref_command = {"user": 'rootie'}

        test_args = rbac.process_args(ref_command)
        assert test_args['user'] == 'rootie'


class TestGeneratePlaybookArgs(object):

    def test_simple_limit(self):
        ref_playbook = 'unicorn.yml'
        ref_limit = ['pink.unicorn.com',
                     'white.unicorn.com',
                     'blue.unicorn.com',
                     'purple.unicorn.com']

        ref_options = {'limit': ref_limit}
        test_args = rbac.generate_args(ref_options)
        assert test_args == ['-l', ':'.join(ref_limit)]

    def test_simple_limit(self):
        ref_tags = ['tag', 'tagg', 'tog', 'togg']

        ref_options = {'tags': ref_tags}

        test_args = rbac.generate_args(ref_options)
        assert test_args == ['-t', ','.join(ref_tags)]

    def test_all(self):

        ref_playbook = 'unicorn.yml'
        ref_limit = ['pink.unicorn.com',
                     'white.unicorn.com',
                     'blue.unicorn.com',
                     'purple.unicorn.com']
        ref_tags = ['tag', 'tagg', 'tog', 'togg']

        ref_options = {'limit': ref_limit,
                       'tags': ref_tags,
                       'check': True,
                       'user': "rootie",
                       "start_at_task": "some task"}
        test_args = rbac.generate_args(ref_options)
        assert test_args == ['-l', ':'.join(ref_limit),
                             '-t', ','.join(ref_tags),
                             '-u', 'rootie',
                             '--check',
                             '--start-at-task="some task"']


class TestDetermineCanRun(object):

    def setup_method(self, method):
        self.ref_acl = {'group/unicorns.yml': ['sysadmin-unicorn', 'sysadmin-pony'],
                        'pony.yml': ['sysadmin-pony']
                        }

    def test_allow_one_of_multiple(self, monkeypatch):
        ref_groups = set(['sysadmin-unicorn'])
        test_can_run = rbac.can_run(self.ref_acl, ref_groups, 'group/unicorns.yml')

        assert test_can_run

    def test_allow_one_of_multiple(self, monkeypatch):
        ref_groups = set(['sysadmin-kittycat'])
        test_can_run = rbac.can_run(self.ref_acl, ref_groups, 'group/unicorns.yml')

        assert not test_can_run


class TestConfigMerge(object):

    def setup_method(self, method):
        self.defaults = copy.deepcopy(rbac.DEFAULT_CONFIG)

    def test_configmerge(self, monkeypatch):
        ref_newconfig = {'config': {'PLAYBOOKS_DIR': '/some/path'}}
        self.defaults['config']['FOOBAR'] = 'BAZ'

        stub_configread = Dingus('configread', return_value=ref_newconfig)
        monkeypatch.setattr(rbac, 'read_configfile', stub_configread)
        monkeypatch.setattr(rbac, 'DEFAULT_CONFIG', self.defaults)

        newconfig = rbac.get_config()

        assert newconfig['config']['PLAYBOOKS_DIR'] == '/some/path'
        assert newconfig['config']['FOOBAR'] == 'BAZ'

    def test_config_add_acls(self, monkeypatch):
        ref_newconfig = {'config': {'PLAYBOOKS_DIR': '/some/path'},
                         'acls': {'group/someplaybook.yml': ['somegroup']}}

        stub_configread = Dingus('configread', return_value=ref_newconfig)
        monkeypatch.setattr(rbac, 'read_configfile', stub_configread)

        newconfig = rbac.get_config()

        assert newconfig['acls']['group/someplaybook.yml'] == ['somegroup']


